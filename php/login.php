<?php
  session_start();

  require("check/db_connect.php");
  require("encript.php");

  if (empty($_POST["login_mail"]) || empty($_POST["login_pass"])) {
    $_SESSION["new_error"] = array(
      "new_error" => "Veuillez remplir les champs ce-dessous."
    );
    header("Location:../user_login.php");
  } else {
    $sql = "SELECT mail_user, pwd_user FROM users WHERE 1";
    $req = $bdd->prepare($sql);
    $req->execute();

    foreach ($req as $user) {
      if (($user["mail_user"] == $_POST["login_mail"]) && ($user["pwd_user"] == encript($_POST["login_pass"]))) {
        $_SESSION["new_succes"] = array(
          "new_succes" => "Vous êtes maintenent connecté"
        );
        setcookie("SESSIONMAITAINER", $_POST["login_mail"], time()+3600*24, "/");
        header("Location:../index.php");
        exit;
      }
    }
    $_SESSION["new_warning"] = array(
      "new_warning" => "Numero d'utilisateur ou mot de passe incorrect."
    );
    header("Location:../user_login.php");
  }
?>

<?php
  if (isset($_SESSION['new_warning'])) {
    foreach ($_SESSION['new_warning'] as $value) {
      echo "
        <div class='alert alert-warning'>
          <button type='button' class='close' data-dismiss='alert'>&times;</button>
          ".$value."
        </div>
      ";
      unset($_SESSION['new_warning']);
    }
  }
?>

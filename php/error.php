<?php
  if (isset($_SESSION['new_error'])) {
    foreach ($_SESSION['new_error'] as $value) {
      echo "
        <div class='alert alert-danger'>
          <button type='button' class='close' data-dismiss='alert'>&times;</button>
          ".$value."
        </div>
      ";
      unset($_SESSION['new_error']);
    }
  }
?>

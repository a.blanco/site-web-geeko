DROP DATABASE rt_geeko_bdd;
CREATE DATABASE rt_geeko_bdd;
USE rt_geeko_bdd;

CREATE TABLE evenements(
  no_event INT NOT NULL,
  type_event VARCHAR(25) NOT NULL,
  desc_event VARCHAR(25) NOT NULL,
  CONSTRAINT PKevenement PRIMARY KEY (no_event)
);

CREATE TABLE users(
  no_user INT(8) NOT NULL,
  nom_user VARCHAR(25) NOT NULL,
  prenom_user VARCHAR(25) NOT NULL,
  mail_user VARCHAR(25) NOT NULL,
  pwd_user VARCHAR(50) NOT NULL,
  status VARCHAR(25) default 'None',
  no_event INT,
  CONSTRAINT PKusers PRIMARY KEY (no_user),
  FOREIGN KEY (no_event) REFERENCES evenements(no_event)
);

CREATE TABLE users_images(
  no_img INT NOT NULL,
  nom_img VARCHAR(50) default "default",
  data_img VARCHAR(500),
  CONSTRAINT PKusers_images PRIMARY KEY (no_img),
  FOREIGN KEY (no_img) REFERENCES users(no_user)
);

CREATE TABLE contact(
  source_demande INT NOT NULL,
  destinataire_demande INT NOT NULL,
  type_demande VARCHAR(25) NOT NULL,
  date_demande VARCHAR(25) NOT NULL,
  message_demande VARCHAR(250) NOT NULL,
  etat VARCHAR(25) NOT NULL default 'non_lu',
  CONSTRAINT PKcontact PRIMARY KEY (source_demande),
  FOREIGN KEY (source_demande) REFERENCES users(no_user)
);

CREATE TABLE pizza(
  no_pizza INT NOT NULL,
  type_pizza INT NOT NULL,
  desc_pizza VARCHAR(50),
  prix_pizza FLOAT NOT NULL,
  CONSTRAINT PKpizza PRIMARY KEY (no_pizza)
);

CREATE TABLE boisson(
  no_boisson INT NOT NULL,
  type_boisson INT NOT NULL,
  desc_boisson VARCHAR(50),
  prix_boisson FLOAT NOT NULL,
  CONSTRAINT PKboisson PRIMARY KEY (no_boisson)
);

CREATE TABLE reservations(
  no_reservation INT NOT NULL,
  no_event INT NOT NULL,
  no_pizza INT,
  no_boisson INT,
  CONSTRAINT PKreservation PRIMARY KEY (no_reservation),
  FOREIGN KEY (no_event) REFERENCES evenements(no_event),
  FOREIGN KEY (no_pizza) REFERENCES pizza(no_pizza),
  FOREIGN KEY (no_boisson) REFERENCES boisson(no_boisson)
);

CREATE TABLE geeko(
  fonds FLOAT default 0,
  nb_membre INT default 0,
  nb_inscrit INT default 0,
  nb_event INT default 0
);

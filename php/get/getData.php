<?php
  function getNumUser(){
    require("db_connect.php");

    $sql = "SELECT no_user FROM users WHERE mail_user=:mail";
    $req = $bdd->prepare($sql);
    $req->execute(array(
      'mail' => $_COOKIE["SESSIONMAITAINER"],
    ));

    foreach ($req as $response) {
      return $response["no_user"];
    }
  }

  function getFirstname(){
    require("db_connect.php");

    $sql = "SELECT nom_user FROM users WHERE mail_user=:mail";
    $req = $bdd->prepare($sql);
    $req->execute(array(
      'mail' => $_COOKIE["SESSIONMAITAINER"],
    ));

    foreach ($req as $response) {
      return $response["nom_user"];
    }
  }

  function getSurname(){
    require("db_connect.php");

    $sql = "SELECT prenom_user FROM users WHERE mail_user=:mail";
    $req = $bdd->prepare($sql);
    $req->execute(array(
      'mail' => $_COOKIE["SESSIONMAITAINER"],
    ));

    foreach ($req as $response) {
      return $response["prenom_user"];
    }
  }

  function getRole(){
    require("db_connect.php");

    $sql = "SELECT status FROM users WHERE mail_user=:mail";
    $req = $bdd->prepare($sql);
    $req->execute(array(
      'mail' => $_COOKIE["SESSIONMAITAINER"],
    ));

    foreach ($req as $response) {
      return $response["status"];
    }
  }

  function getNbEvents(){
    require("db_connect.php");

    $sql = "SELECT COUNT(no_event) AS nb FROM evenements WHERE 1";
    $req = $bdd->prepare($sql);
    $req->execute();

    foreach ($req as $response) {
      return $response["nb"];
    }
  }

  function getImg(){
    require("db_connect.php");

    $sql = "SELECT nom_img FROM users_images i, users u WHERE i.no_img=u.no_user";
    $req = $bdd->prepare($sql);
    $req->execute();

    foreach ($req as $response) {
      return $response["nom_img"];
    }
  }
?>

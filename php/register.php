<?php
  session_start();

  require("check/db_connect.php");
  require("encript.php");

  $error = 0;

  if (empty($_POST["register_numstudent"]) || empty($_POST["register_firstname"]) || empty($_POST["register_lastname"]) || empty($_POST["register_mail"]) || empty($_POST["register_pass1"]) || empty($_POST["register_pass2"])) {
    $error = 1;
    $_SESSION["new_warning"] = array(
      "new_warning" => "Veuillez remplir tous les champs ci-dessous."
    );
    header("Location:../user_register.php");
  } else {
    if (encript($_POST["register_pass1"]) != encript($_POST["register_pass2"])) {
      $error = 1;
      $_SESSION["new_error"] = array(
        "new_error" => "Les mots de passes ne correspondent pas"
      );
      header("Location:../user_register.php");
    } else if (!filter_var($_POST["register_mail"], FILTER_VALIDATE_EMAIL)) {
      $error = 1;
      $_SESSION["new_error"] = array(
        "new_error" => "L'adresse mail n'est pas correct"
      );
      header("Location:../user_register.php");
    } else {
      $sql = "SELECT no_user, mail_user FROM users WHERE 1";
      $req = $bdd->prepare($sql);
      $req->execute();

      foreach ($req as $user) {
        if ($user['no_user'] == $_POST["register_numstudent"] && $user['mail_user'] == $_POST["register_mail"]) {
          $error = 1;
          $_SESSION["new_error"] = array(
            "new_error" => "Le numero d'etudiant $_POST[register_numstudent] et l'adresse mail $_POST[register_mail] sont déjà utilisés. Contacter un administrateur du site <a href='contact.php'><u>ici</u></a>"
          );
          header("Location:../user_register.php");
          exit;
        } else if ($user['no_user'] == $_POST["register_numstudent"]) {
          $error = 1;
          $_SESSION["new_error"] = array(
            "new_error" => "Le numero d'etudiant $_POST[register_numstudent] est déjà utilisé. Contacter un administrateur du site <a href='contact.php'><u>ici</u></a>"
          );
          header("Location:../user_register.php");
          exit;
        } else if ($user['mail_user'] == $_POST["register_mail"]) {
          $error = 1;
          $_SESSION["new_error"] = array(
            "new_error" => "L'adresse mail $_POST[register_mail] est déjà utilisé. Contacter un administrateur du site <a href='contact.php'><u>ici</u></a>"
          );
          header("Location:../user_register.php");
          exit;
        }
      }

      if ($error == 0) {
        $sql = "INSERT INTO users(no_user, nom_user, prenom_user, mail_user, pwd_user) VALUES (:no_user, :nom_user, :prenom_user, :mail_user, :pwd_user)";
        $req = $bdd->prepare($sql);
        $req->execute(array(
          'no_user' => $_POST["register_numstudent"],
          'nom_user' => strtoupper($_POST["register_lastname"]),
          'prenom_user' => ucfirst(strtolower($_POST["register_firstname"])),
          'mail_user' => $_POST["register_mail"],
          'pwd_user' => encript($_POST["register_pass1"]),
        ));
        $_SESSION["new_succes"] = array(
          "new_succes" => "Un mail de confirmation vous a été envoyé afin de finalisé votre inscription."
        );
        header("Location:../user_login.php");
      }

      //echo "Data =";
      //echo $_POST["register_numstudent"] . ' ' . strtoupper($_POST["register_lastname"]) . ' ' . ucfirst(strtolower($_POST["register_firstname"])) . ' ' . $_POST["register_mail"] . ' ' . encript($_POST["register_pass1"]);
    }
  }
?>

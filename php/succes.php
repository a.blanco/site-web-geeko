<?php
  if (isset($_SESSION['new_succes'])) {
    foreach ($_SESSION['new_succes'] as $value) {
      echo "
        <div class='alert alert-success'>
          <button type='button' class='close' data-dismiss='alert'>&times;</button>
          ".$value."
        </div>
      ";
      unset($_SESSION['new_succes']);
    }
  }
?>

<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Geeko - Forgot Password</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/geeko-style.css" rel="stylesheet">

  <!-- Script : reCaptcha-->
  <script src="https://www.google.com/recaptcha/api.js?render=6LcZJ5MUAAAAAFc1JpQzMRKI0cOW6OcAEl_rmf4I"></script>
  <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcZJ5MUAAAAAFc1JpQzMRKI0cOW6OcAEl_rmf4I', {action: 'homepage'});
    });
  </script>

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Mot de passe oublié ?</h1>
                    <p class="mb-4">Ne vous inquiètez pas. Saisissez simplement voutre adresse mail ci-dessous afin recevoir un mail de reinitialisation.</p>
                    <?php require("php/succes.php"); ?>
                    <?php require("php/warning.php"); ?>
                    <?php require("php/error.php"); ?>
                  </div>
                  <form class="user" action="php/forgot-password.php" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Adresse mail..." name="forgot-password_mail">
                    </div>
                    <button class="btn btn-primary btn-user btn-block" type="submit">
                      Envoyer
                    </button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a href="index.php" class="btn btn-icon-split-secondary bg-gray-300 btn-user btn-block">
                      Annuler
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/geeko.js"></script>

</body>

</html>
